<?php

class HomeCest {

    public function homepageWorks(AcceptanceTester $I) {
        $I->amOnPage('/');
        $I->seeInTitle('php-gitlab-ci-codeception-selenium');
        $I->see('PHP Version 7.4.7', 'h1');
        $I->makeScreenshot();
    }

}
